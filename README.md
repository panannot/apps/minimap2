# minimap2 Singularity container
### Package minimap2 Version 2.28
A versatile pairwise aligner for genomic and spliced nucleotide sequences.
Docs: https://lh3.github.io/minimap2/minimap2.html

Homepage:

https://github.com/lh3/minimap2

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
minimap2 Version: 2.28<br>
Singularity container based on the recipe: Singularity.minimap2_v2.28.def

Local build:
```
sudo singularity build minimap2_v2.28.sif Singularity.minimap2_v2.28.def
```

Get image help:
```
singularity run-help minimap2_v2.28.sif
```

Default runscript: minimap2<br>
Usage:
```
./minimap2_v2.28.sif --help
```
or:
```
singularity exec minimap2_v2.28.sif minimap2 --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull minimap2_v2.28.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/minimap2/minimap2:latest

```

